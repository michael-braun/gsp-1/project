//
// Created by michael on 07.05.19.
//

#ifndef VORLESUNG2_STRINGEXCEPTION_H
#define VORLESUNG2_STRINGEXCEPTION_H

#include <string>

class StringException : std::exception {
private:
    std::string message;
public:
    explicit StringException(const std::string& message) {
        this->message = message;
    }

    const std::string& getMessage() {
        return message;
    }
};


#endif //VORLESUNG2_STRINGEXCEPTION_H
