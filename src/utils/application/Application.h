#include <SDL2/SDL.h>
#include <GL/glew.h>

//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_APPLICATION_H
#define VORLESUNG2_APPLICATION_H


class Application {
private:
    bool running = false;
    int exitCode = 0;
public:
    Application();
    int run();
    virtual void setup() { };
    virtual void loop() { exit(0); };
    void exit(int code);
    ~Application();
};


#endif //VORLESUNG2_APPLICATION_H
