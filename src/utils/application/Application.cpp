//
// Created by michael on 16.04.19.
//

#include "Application.h"

Application::Application() {

}

int Application::run() {
    this->running = true;

    this->setup();

    while(this->running) {
        this->loop();
    }

    return this->exitCode;
}

void Application::exit(int code) {
    this->running = false;
    this->exitCode = code;
}

Application::~Application() {

}
