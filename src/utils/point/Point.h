//
// Created by michael on 06.05.19.
//

#ifndef VORLESUNG2_POINT_H
#define VORLESUNG2_POINT_H

#include <glm/vec3.hpp>

class Point : public glm::vec3 {
public:
    Point(float x, float y, float z) : glm::vec3(x, y, z) {};
};


#endif //VORLESUNG2_POINT_H
