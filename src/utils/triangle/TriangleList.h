//
// Created by michael on 07.05.19.
//

#ifndef VORLESUNG2_TRIANGLELIST_H
#define VORLESUNG2_TRIANGLELIST_H


#include <vector>
#include <memory>
#include "Triangle.h"
#include "../Program.h"
#include "../vertex-array/VertexArray.h"
#include "../buffer/Buffer.h"
#include "TriangleVector.h"
#include "../transformation/TransformationList.h"

class TriangleList {
private:
    bool locked = false;
    std::vector<TriangleVector> triangles;
    std::unique_ptr<Program> program;
    std::unique_ptr<VertexArray> vertexArray;
    std::unique_ptr<Buffer> buffer;
    std::reference_wrapper<const TransformationList> transformations;
public:
    TriangleList(): TriangleList(TransformationList::EMPTY) {}
    TriangleList(const TransformationList& trans);
    void setup();
    void draw();
    void add(const Triangle& triangle);
    void setTransformations(const TransformationList& transformations) { this->transformations = transformations; }
};


#endif //VORLESUNG2_TRIANGLELIST_H
