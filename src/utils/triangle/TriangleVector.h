//
// Created by michael on 13.05.19.
//

#ifndef VORLESUNG2_TRIANGLEVECTOR_H
#define VORLESUNG2_TRIANGLEVECTOR_H


#include <GL/glew.h>
#include "Triangle.h"
#include <glm/vec3.hpp>

using glm::vec3;

class TriangleVector {
private:
    Point p1;
    vec3 v1;
    Point p2;
    vec3 v2;
    Point p3;
    vec3 v3;
public:
    TriangleVector(const Triangle &triangle);
    static const int OFFSET = 6 * sizeof(GL_FLOAT);
};


#endif //VORLESUNG2_TRIANGLEVECTOR_H
