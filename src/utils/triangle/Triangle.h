//
// Created by michael on 06.05.19.
//

#ifndef VORLESUNG2_TRIANGLE_H
#define VORLESUNG2_TRIANGLE_H


#include "../point/Point.h"

class Triangle {
private:
    Point p1;
    Point p2;
    Point p3;

public:
    Triangle(Point p1, Point p2, Point p3) : p1(p1), p2(p2), p3(p3) {};

    const Point& getP1() const;
    const Point& getP2() const;
    const Point& getP3() const;
};


#endif //VORLESUNG2_TRIANGLE_H
