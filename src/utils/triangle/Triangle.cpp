//
// Created by michael on 06.05.19.
//

#include "Triangle.h"

const Point &Triangle::getP1() const {
    return this->p1;
}

const Point &Triangle::getP2() const {
    return this->p2;
}

const Point &Triangle::getP3() const {
    return this->p3;
}
