//
// Created by michael on 07.05.19.
//

#include <string>
#include <math.h>
#include "TriangleList.h"
#include "../shader/FragmentShader.h"
#include "../shader/VertexShader.h"
#include "../Color.h"
#include "../error/StringException.h"
#include "../transformation/Transformation.h"
#include "../transformation/rotation/RotationTransformation.h"
#include "../transformation/translation/TranslationTransformation.h"
#include "../transformation/scale/ScaleTransformation.h"
#include "../transformation/TransformationList.h"

#include <glm/glm.hpp>

#define PI 3.14159265

TriangleList::TriangleList(const TransformationList& trans) : transformations(trans) {
    program = std::make_unique<Program>();
    if (program == nullptr) {
        throw("Could not construct program.");
    }

    buffer = std::make_unique<Buffer>();
    if (buffer == nullptr) {
        throw("Could not construct buffer.");
    }

    vertexArray = std::make_unique<VertexArray>();
    if (vertexArray == nullptr) {
        throw("Could not construct vertexArray.");
    }
}

void TriangleList::setup() {
    this->locked = true;

    VertexShader vertexShader(R"(
        #version 330 core

        layout(location = 0) in vec3 vertex_position;
        layout(location = 1) in vec3 vertex_normal;

        out vec3 vertex_normal_worldspace;
        out vec3 fragment_position_worldspace;

        uniform mat4 model_to_world_matrix;

        void main() {
            gl_Position.xyz = vertex_position;
            gl_Position.w = 1.0;

            gl_Position = model_to_world_matrix * gl_Position;
            fragment_position_worldspace = gl_Position.xyz;
            vertex_normal_worldspace = (inverse(transpose(model_to_world_matrix)) * vec4(vertex_normal, 1)).xyz;
        }
    )");
    FragmentShader fragmentShader(R"(
        #version 330 core

        uniform vec4 user_color;

        layout(location = 0) out vec3 color;

        in vec3 vertex_normal_worldspace;
        in vec3 fragment_position_worldspace;

        uniform vec3 light_pos;

        void main() {
            vec3 norm = normalize(vertex_normal_worldspace);
            vec3 lightDir = normalize(light_pos - fragment_position_worldspace);

            float diff = max(dot(norm, lightDir), 0.0);
            vec3 diffuse = diff * vec3(1, 1, 1);

            vec3 factor = vec3(0.1, 0.1, 0.1) + diffuse;
            color = factor * user_color.xyz;
        }
    )");
    program->attach(vertexShader);
    program->attach(fragmentShader);
    program->link();

    this->vertexArray->bind();
    this->buffer->bind();

    glBufferData(GL_ARRAY_BUFFER, triangles.size() * sizeof(TriangleVector), triangles.data(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, false, TriangleVector::OFFSET, 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, false, TriangleVector::OFFSET, (GLvoid*)(3 * sizeof(GL_FLOAT)));

    RotationTransformation rotMat(0, 0, 0);
    ScaleTransformation scaleMat(0.5);
    TranslationTransformation transMat(0, 0.5, 0);

    TransformationList transList;
    transList.add(rotMat);
    transList.add(scaleMat);
    transList.add(transMat);
//    this->setTransformations(transList);

    rotMat.setRotation(0, 45, 0);
}

void TriangleList::add(const Triangle &triangle) {
    if (this->locked) {
        throw("TriangleList already locked");
    }

    this->triangles.emplace_back(triangle);
}

void TriangleList::draw() {
    program->use();

    program->setUniformMatrix("model_to_world_matrix", ((const TransformationList&) this->transformations).toMatrix());
    program->setUniformVector("user_color", Color(0.443, 0.694, 0.153, 0).toVector());
    program->setUniformVector("light_pos", Point(-10, 10, 10));
//    program->setUniformVector("user_color", Color::WHITE.toVector());

    glDrawArrays(GL_TRIANGLES, 0, 3 * triangles.size());
    GLenum errorCode = glGetError();
    if (errorCode != GL_NO_ERROR) {
        throw(StringException(std::to_string(errorCode)));
    }
}
