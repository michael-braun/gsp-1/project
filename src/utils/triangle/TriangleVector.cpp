//
// Created by michael on 13.05.19.
//

#include <glm/geometric.hpp>
#include "TriangleVector.h"

TriangleVector::TriangleVector(const Triangle &triangle) : p1(triangle.getP1()), p2(triangle.getP2()), p3(triangle.getP3()) {
    Point a = triangle.getP1();
    Point b = triangle.getP2();
    Point c = triangle.getP3();
    glm::vec3 normale = glm::normalize(glm::cross((b - a), (c - a)));

    this->v1 = normale;
    this->v2 = normale;
    this->v3 = normale;
}
