//
// Created by michael on 30.04.19.
//

#ifndef VORLESUNG2_PROGRAM_H
#define VORLESUNG2_PROGRAM_H

#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

#include "shader/Shader.h"

using glm::mat4x4;

class Program {
private:
    GLuint id;
public:
    Program();
    void attach(Shader &shader);
    void link();
    void use() const;
    GLint getUniformLocation(const GLchar* name);
    void setUniformMatrix(const char* name, const mat4x4 &matrix);
    void setUniformMatrix(GLint loc, const mat4x4 &matrix);
    void setUniformVector(const char* name, const glm::vec3 &vector);
    void setUniformVector(GLint loc, const glm::vec3 &vector);
    void setUniformVector(const char* name, const glm::vec4 &vector);
    void setUniformVector(GLint loc, const glm::vec4 &vector);
    void setUniform(const char* name, GLfloat data);
    void setUniform(GLint loc, GLfloat data);
    ~Program();
};


#endif //VORLESUNG2_PROGRAM_H
