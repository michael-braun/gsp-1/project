//
// Created by michael on 30.04.19.
//

#include "Program.h"

Program::Program() : id(glCreateProgram()) {}

void Program::attach(Shader &shader) {
    glAttachShader(this->id, shader.getId());
}

void Program::link() {
    glLinkProgram(this->id);
}

void Program::use() const {
    glUseProgram(this->id);
}

GLint Program::getUniformLocation(const GLchar *name) {
    return glGetUniformLocation(this->id, name);
}

void Program::setUniformMatrix(const char* name, const mat4x4 &matrix) {
    GLint loc = this->getUniformLocation(name);
    return this->setUniformMatrix(loc, matrix);
}

void Program::setUniformMatrix(GLint loc, const mat4x4 &matrix) {
    glUniformMatrix4fv(loc, 1, GL_FALSE, (const float*) &matrix);
}

Program::~Program() {
    glDeleteProgram(this->id);
}

void Program::setUniformVector(GLint loc, const glm::vec3 &vector) {
    glUniform3fv(loc, 1, (float*) &vector);
}

void Program::setUniformVector(const char *name, const glm::vec3 &vector) {
    GLint loc = this->getUniformLocation(name);
    return this->setUniformVector(loc, vector);
}

void Program::setUniformVector(GLint loc, const glm::vec4 &vector) {
    glUniform4fv(loc, 1, (float*) &vector);
}

void Program::setUniformVector(const char *name, const glm::vec4 &vector) {
    GLint loc = this->getUniformLocation(name);
    return this->setUniformVector(loc, vector);
}

void Program::setUniform(GLint loc, GLfloat data) {
    glUniform1f(loc, data);
}

void Program::setUniform(const char *name, GLfloat data) {
    GLint loc = this->getUniformLocation(name);
    glUniform1f(loc, data);
}
