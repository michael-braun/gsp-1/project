//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_WINDOW_H
#define VORLESUNG2_WINDOW_H


#include <SDL2/SDL.h>
#include "application/Application.h"
#include "Color.h"
#include "Program.h"
#include "event/WindowEventHandler.h"

class Window {
private:
    Color color = Color::BLACK;
    SDL_Window* window = nullptr;
    SDL_GLContext context = nullptr;
    WindowEventHandler* eventHandler = nullptr;
public:
    Window();
    void setBackgroundColor(const Color &color);
    void draw();
    void show();
    void clear();
    void swap();
    void use(WindowEventHandler* eventHandler);
    ~Window();
};


#endif //VORLESUNG2_WINDOW_H
