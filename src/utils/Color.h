//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_COLOR_H
#define VORLESUNG2_COLOR_H

#include <glm/vec4.hpp>
#include <GL/glew.h>

class Color {
    GLfloat r;
    GLfloat g;
    GLfloat b;
    GLfloat a;

public:
    Color(GLfloat r, GLfloat g, GLfloat b);
    Color(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

    GLfloat getRed();
    GLfloat getGreen();
    GLfloat getBlue();
    GLfloat getAlpha();

    glm::vec4 toVector();

    static Color WHITE;
    static Color BLACK;
    static Color RED;
    static Color GREEN;
    static Color BLUE;
};


#endif //VORLESUNG2_COLOR_H
