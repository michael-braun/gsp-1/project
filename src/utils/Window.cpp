//
// Created by michael on 16.04.19.
//

#include <GL/glew.h>
#include <iostream>

#include "Window.h"
#include "Program.h"

Window::Window() :
    window(SDL_CreateWindow("Test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL))
{
    this->context = SDL_GL_CreateContext(this->window);
    if (this->context == nullptr) {
        throw("Could not initialize OpenGL-context");
    }

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
        throw("Could not initialize GLEW");
    }
}

void Window::show() {
    SDL_ShowWindow(this->window);
}

void Window::setBackgroundColor(const Color &pColor) {
    this->color = pColor;
}

void Window::swap() {
    SDL_GL_SwapWindow(this->window);
}

void Window::clear() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::draw() {
    this->clear();
    glClearColor(this->color.getRed(), this->color.getGreen(), this->color.getBlue(), this->color.getAlpha());
    // glBindVertexArray (opt.)

    if (this->eventHandler != nullptr) {
        this->eventHandler->onBeforeSwap();
    }

    this->swap();
}

Window::~Window() {
    SDL_GL_DeleteContext(this->context);
    SDL_DestroyWindow(this->window);
}

void Window::use(WindowEventHandler* windowEventHandler) {
    this->eventHandler = windowEventHandler;
}
