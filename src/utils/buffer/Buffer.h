//
// Created by michael on 07.05.19.
//

#ifndef VORLESUNG2_BUFFER_H
#define VORLESUNG2_BUFFER_H


#include <GL/glew.h>

class Buffer {
private:
    GLuint id = 0;
public:
    Buffer();
    void bind();
    ~Buffer();
};


#endif //VORLESUNG2_BUFFER_H
