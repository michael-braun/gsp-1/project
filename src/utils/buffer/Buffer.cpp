//
// Created by michael on 07.05.19.
//

#include "Buffer.h"

Buffer::Buffer() {
    glGenBuffers(1, &this->id);
}

void Buffer::bind() {
    glBindBuffer(GL_ARRAY_BUFFER, this->id);
}

Buffer::~Buffer() {
    glDeleteBuffers(1, &this->id);
}
