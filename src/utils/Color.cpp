//
// Created by michael on 16.04.19.
//

#include "Color.h"

Color::Color(GLfloat r, GLfloat g, GLfloat b) : Color(r, g, b, 0) {}

Color::Color(GLfloat r, GLfloat g, GLfloat b, GLfloat a) {
    this->r = r;
    this->g = g;
    this->b = b;
    this->a = a;
}

GLfloat Color::getRed() {
    return this->r;
}

GLfloat Color::getGreen() {
    return this->g;
}

GLfloat Color::getBlue() {
    return this->b;
}

GLfloat Color::getAlpha() {
    return this->a;
}

glm::vec4 Color::toVector() {
    return glm::vec4(this->r, this->g, this->b, this->a);
}

Color Color::WHITE { 1, 1, 1 };
Color Color::BLACK { 0, 0, 0 };
Color Color::RED = { 1, 0, 0 };
Color Color::GREEN = { 0, 1, 0 };
Color Color::BLUE = { 0, 0, 1 };
