//
// Created by michael on 07.05.19.
//

#include "VertexArray.h"

VertexArray::VertexArray() {
    glGenVertexArrays(1, &this->id);
}

void VertexArray::bind() {
    glBindVertexArray(this->id);
}

VertexArray::~VertexArray() {
    glDeleteVertexArrays(1, &this->id);
}
