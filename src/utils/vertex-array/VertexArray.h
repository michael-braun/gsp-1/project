//
// Created by michael on 07.05.19.
//

#ifndef VORLESUNG2_VERTEXARRAY_H
#define VORLESUNG2_VERTEXARRAY_H


#include <GL/glew.h>

class VertexArray {
private:
    GLuint id = 0;
public:
    VertexArray();
    void bind();
    ~VertexArray();
};


#endif //VORLESUNG2_VERTEXARRAY_H
