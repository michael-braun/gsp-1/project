//
// Created by michael on 16.04.19.
//

#include <SDL2/SDL_events.h>
#include "EventManager.h"

EventManager::EventManager(EventHandler *eventHandler) {
    this->handler = eventHandler;
}

void EventManager::process() {
    SDL_Event event;

    while(SDL_PollEvent(&event)) {
        this->handler->onEvent(event);

        switch (event.type) {
            case SDL_QUIT:
                this->handler->onExit(event.quit);
                break;
            case SDL_KEYDOWN:
                this->handler->onKeyDown(event.key);
                break;
            case SDL_KEYUP:
                this->handler->onKeyUp(event.key);
                break;
        }
    }
}
