//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_EVENTHANDLER_H
#define VORLESUNG2_EVENTHANDLER_H


#include <SDL2/SDL_events.h>

class EventHandler {
public:
    virtual void onEvent(const SDL_Event& event) { };
    virtual void onExit(const SDL_QuitEvent& event) { };
    virtual void onKeyDown(const SDL_KeyboardEvent& event) { };
    virtual void onKeyUp(const SDL_KeyboardEvent& event) { };
};


#endif //VORLESUNG2_EVENTHANDLER_H
