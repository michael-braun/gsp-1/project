//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_EVENTMANAGER_H
#define VORLESUNG2_EVENTMANAGER_H


#include "EventHandler.h"

class EventManager {
    EventHandler* handler;

public:
    EventManager(EventHandler* eventHandler);
    void process();
};


#endif //VORLESUNG2_EVENTMANAGER_H
