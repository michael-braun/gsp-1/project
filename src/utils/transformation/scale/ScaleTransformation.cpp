//
// Created by michael on 18.05.19.
//

#include "ScaleTransformation.h"

ScaleTransformation::ScaleTransformation(float scaleX, float scaleY, float scaleZ) {
    this->scaleX = scaleX;
    this->scaleY = scaleY;
    this->scaleZ = scaleZ;

    (*this)[3].w = 1;

    this->updateMatrix();
}

void ScaleTransformation::updateMatrix() {
    (*this)[0].x = this->scaleX;
    (*this)[1].y = this->scaleY;
    (*this)[2].z = this->scaleZ;
}

void ScaleTransformation::setScale(float scaleX, float scaleY, float scaleZ) {
    this->scaleX = scaleX;
    this->scaleY = scaleY;
    this->scaleZ = scaleZ;

    this->updateMatrix();
}

void ScaleTransformation::setScaleX(float scaleX) {
    this->scaleX = scaleX;

    this->updateMatrix();
}

void ScaleTransformation::setScaleY(float scaleY) {
    this->scaleY = scaleY;

    this->updateMatrix();
}

void ScaleTransformation::setScaleZ(float scaleZ) {
    this->scaleZ = scaleZ;

    this->updateMatrix();
}
