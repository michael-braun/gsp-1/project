//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_SCALETRANSFORMATION_H
#define VORLESUNG2_SCALETRANSFORMATION_H


#include "../Transformation.h"

class ScaleTransformation : public Transformation {
private:
    float scaleX;
    float scaleY;
    float scaleZ;

    void updateMatrix();
public:
    ScaleTransformation() : ScaleTransformation(1) {};
    explicit ScaleTransformation(float scale): ScaleTransformation(scale, scale, scale) {};
    ScaleTransformation(float scaleX, float scaleY, float scaleZ);
    void setScale(float scale) { return setScale(scale, scale, scale); };
    void setScale(float scaleX, float scaleY, float scaleZ);
    void setScaleX(float scaleX);
    void setScaleY(float scaleY);
    void setScaleZ(float scaleZ);
};


#endif //VORLESUNG2_SCALETRANSFORMATION_H
