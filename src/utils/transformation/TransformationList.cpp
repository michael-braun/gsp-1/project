//
// Created by michael on 18.05.19.
//

#include "TransformationList.h"

mat4x4 TransformationList::toMatrix() const {
    mat4x4 matrix(1.0f);

    for(int i = 0, len = this->size(); i < len; i++) {
        matrix = matrix * this->get(i);
    }

    return matrix;
}

TransformationList TransformationList::EMPTY = TransformationList();
