//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_TRANSFORMATIONLIST_H
#define VORLESUNG2_TRANSFORMATIONLIST_H

#include <vector>
#include <functional>
#include <glm/mat4x4.hpp>
#include "./Transformation.h"

using glm::mat4x4;

class TransformationList {
private:
    std::vector<std::reference_wrapper<const Transformation>> transformations;
public:
    mat4x4 toMatrix() const;
    void add(const Transformation& trans) { transformations.push_back(std::ref(trans)); }
    void remove(int i) { transformations.erase(transformations.begin() + i); }
    const Transformation& get(int i) const { return transformations[i]; }
    int size() const { return transformations.size(); }

    static TransformationList EMPTY;
};


#endif //VORLESUNG2_TRANSFORMATIONLIST_H
