//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_TRANSLATIONTRANSFORMATION_H
#define VORLESUNG2_TRANSLATIONTRANSFORMATION_H


#include "../Transformation.h"

class TranslationTransformation : public Transformation {
private:
    float translateX;
    float translateY;
    float translateZ;

    void updateMatrix();
public:
    TranslationTransformation() : TranslationTransformation(0, 0, 0) {};
    TranslationTransformation(float x, float y, float z);
    void setTranslation(float x, float y, float z);
    void setX(float x);
    void setY(float y);
    void setZ(float z);
};


#endif //VORLESUNG2_TRANSLATIONTRANSFORMATION_H
