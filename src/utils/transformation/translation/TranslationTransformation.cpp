//
// Created by michael on 18.05.19.
//

#include "TranslationTransformation.h"

TranslationTransformation::TranslationTransformation(float x, float y, float z) {
    this->translateX = x;
    this->translateY = y;
    this->translateZ = z;

    (*this)[0].x = 1;
    (*this)[1].y = 1;
    (*this)[2].z = 1;
    (*this)[3].w = 1;

    this->updateMatrix();
}

void TranslationTransformation::updateMatrix() {
    (*this)[3].x = this->translateX;
    (*this)[3].y = this->translateY;
    (*this)[3].z = this->translateZ;
}

void TranslationTransformation::setTranslation(float x, float y, float z) {
    this->translateX = x;
    this->translateY = y;
    this->translateZ = z;

    this->updateMatrix();
}

void TranslationTransformation::setX(float x) {
    this->translateX = x;

    this->updateMatrix();
}

void TranslationTransformation::setY(float y) {
    this->translateY = y;

    this->updateMatrix();
}

void TranslationTransformation::setZ(float z) {
    this->translateZ = z;

    this->updateMatrix();
}
