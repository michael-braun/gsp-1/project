//
// Created by michael on 14.05.19.
//

#ifndef VORLESUNG2_TRANSFORMATION_H
#define VORLESUNG2_TRANSFORMATION_H

#include <glm/mat4x4.hpp>

using glm::mat4x4;

class Transformation : public mat4x4 {
public:
    Transformation() : mat4x4( 0.0f ) {};
    Transformation(
        const float &x0, const float &y0, const float &z0, const float &w0,
        const float &x1, const float &y1, const float &z1, const float &w1,
        const float &x2, const float &y2, const float &z2, const float &w2,
        const float &x3, const float &y3, const float &z3, const float &w3
    ) : mat4x4(
        x0, y0, z0, w0,
        x1, y1, z1, w1,
        x2, y2, z2, w2,
        x3, y3, z3, w3
    ) {};
};


#endif //VORLESUNG2_TRANSFORMATION_H
