//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_ROTATIONTRANSFORMATION_H
#define VORLESUNG2_ROTATIONTRANSFORMATION_H


#include "../Transformation.h"
#include "RotationXTransformation.h"
#include "RotationYTransformation.h"
#include "RotationZTransformation.h"

class RotationTransformation : public Transformation {
private:
    RotationXTransformation rotX;
    RotationYTransformation rotY;
    RotationZTransformation rotZ;

    void updateMatrix();
public:
    RotationTransformation() : RotationTransformation(0) {}
    explicit RotationTransformation(float rot) : RotationTransformation(rot, rot, rot) {}
    RotationTransformation(float rotX, float rotY, float rotZ);
    void setRotation(float rotation) { return setRotation(rotation, rotation, rotation); };
    void setRotation(float rotX, float rotY, float rotZ);
    void setRotationX(float rotX);
    void setRotationY(float rotY);
    void setRotationZ(float rotZ);
};


#endif //VORLESUNG2_ROTATIONTRANSFORMATION_H
