//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_ROTATIONYTRANSFORMATION_H
#define VORLESUNG2_ROTATIONYTRANSFORMATION_H


#include "AxisRotationTransformation.h"

class RotationYTransformation : public AxisRotationTransformation {
protected:
    void updateMatrix() final;
public:
    RotationYTransformation() : AxisRotationTransformation() { this->updateMatrix(); }
    explicit RotationYTransformation(float rotation) : AxisRotationTransformation(rotation) { this->updateMatrix(); }
};


#endif //VORLESUNG2_ROTATIONYTRANSFORMATION_H
