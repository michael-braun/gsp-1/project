//
// Created by michael on 18.05.19.
//

#include <math.h>
#include "RotationYTransformation.h"

#define PI 3.14159265

void RotationYTransformation::updateMatrix() {
    float rotation = this->getRotation();

    (*this)[0].x = std::cos(rotation * PI/180);
    (*this)[0].y = 0;
    (*this)[0].z = -std::sin(rotation * PI/180);
    (*this)[0].w = 0;

    (*this)[1].x = 0;
    (*this)[1].y = 1;
    (*this)[1].z = 0;
    (*this)[1].w = 0;

    (*this)[2].x = std::sin(rotation * PI/180);
    (*this)[2].y = 0;
    (*this)[2].z = std::cos(rotation * PI/180);
    (*this)[2].w = 0;

    (*this)[3].x = 0;
    (*this)[3].y = 0;
    (*this)[3].z = 0;
    (*this)[3].w = 1;
}
