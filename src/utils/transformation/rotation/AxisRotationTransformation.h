//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_AXISROTATIONTRANSFORMATION_H
#define VORLESUNG2_AXISROTATIONTRANSFORMATION_H


#include "../Transformation.h"

class AxisRotationTransformation : public Transformation {
private:
    float rotation;
protected:
    virtual void updateMatrix() = 0;
public:
    AxisRotationTransformation() : AxisRotationTransformation(0) {}
    explicit AxisRotationTransformation(float rotation);
    float getRotation();
    void setRotation(float rotation);
};


#endif //VORLESUNG2_AXISROTATIONTRANSFORMATION_H
