//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_ROTATIONZTRANSFORMATION_H
#define VORLESUNG2_ROTATIONZTRANSFORMATION_H


#include "./AxisRotationTransformation.h"

class RotationZTransformation : public AxisRotationTransformation {
protected:
    void updateMatrix() final;
public:
    RotationZTransformation() : AxisRotationTransformation() { this->updateMatrix(); }
    explicit RotationZTransformation(float rotation) : AxisRotationTransformation(rotation) { this->updateMatrix(); }
};


#endif //VORLESUNG2_ROTATIONZTRANSFORMATION_H
