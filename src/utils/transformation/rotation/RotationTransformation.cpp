//
// Created by michael on 18.05.19.
//

#include "RotationTransformation.h"

#include <math.h>

#define PI 3.14159265

RotationTransformation::RotationTransformation(float rotX, float rotY, float rotZ) : rotX(rotX), rotY(rotY), rotZ(rotZ) {
    this->updateMatrix();
}

void RotationTransformation::updateMatrix() {
    mat4x4 trans = this->rotX * this->rotY * this->rotZ;

    (*this)[0].x = trans[0].x;
    (*this)[0].y = trans[0].y;
    (*this)[0].z = trans[0].z;
    (*this)[0].w = trans[0].w;

    (*this)[1].x = trans[1].x;
    (*this)[1].y = trans[1].y;
    (*this)[1].z = trans[1].z;
    (*this)[1].w = trans[1].w;

    (*this)[2].x = trans[2].x;
    (*this)[2].y = trans[2].y;
    (*this)[2].z = trans[2].z;
    (*this)[2].w = trans[2].w;

    (*this)[3].x = trans[3].x;
    (*this)[3].y = trans[3].y;
    (*this)[3].z = trans[3].z;
    (*this)[3].w = trans[3].w;
}

void RotationTransformation::setRotation(float rotX, float rotY, float rotZ) {
    this->rotX.setRotation(rotX);
    this->rotY.setRotation(rotY);
    this->rotZ.setRotation(rotZ);

    this->updateMatrix();
}

void RotationTransformation::setRotationX(float rotX) {
    this->rotX.setRotation(rotX);

    this->updateMatrix();
}

void RotationTransformation::setRotationY(float rotY) {
    this->rotY.setRotation(rotY);

    this->updateMatrix();
}

void RotationTransformation::setRotationZ(float rotZ) {
    this->rotZ.setRotation(rotZ);

    this->updateMatrix();
}
