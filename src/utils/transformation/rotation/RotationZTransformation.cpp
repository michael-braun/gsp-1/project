//
// Created by michael on 18.05.19.
//

#include <math.h>
#include "RotationZTransformation.h"

#define PI 3.14159265

void RotationZTransformation::updateMatrix() {
    float rotation = this->getRotation();

    (*this)[0].x = std::cos(rotation * PI/180);
    (*this)[0].y = std::sin(rotation * PI/180);
    (*this)[0].z = 0;
    (*this)[0].w = 0;

    (*this)[1].x = -std::sin(rotation * PI/180);
    (*this)[1].y = std::cos(rotation * PI/180);
    (*this)[1].z = 0;
    (*this)[1].w = 0;

    (*this)[2].x = 0;
    (*this)[2].y = 0;
    (*this)[2].z = 1;
    (*this)[2].w = 0;

    (*this)[3].x = 0;
    (*this)[3].y = 0;
    (*this)[3].z = 0;
    (*this)[3].w = 1;
}
