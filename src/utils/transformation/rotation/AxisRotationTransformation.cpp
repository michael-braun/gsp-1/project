//
// Created by michael on 18.05.19.
//

#include "AxisRotationTransformation.h"

AxisRotationTransformation::AxisRotationTransformation(float rotation) : rotation(rotation) {}

float AxisRotationTransformation::getRotation() {
    return this->rotation;
}

void AxisRotationTransformation::setRotation(float rotation) {
    this->rotation = rotation;

    this->updateMatrix();
}
