//
// Created by michael on 18.05.19.
//

#ifndef VORLESUNG2_ROTATIONXTRANSFORMATION_H
#define VORLESUNG2_ROTATIONXTRANSFORMATION_H


#include "AxisRotationTransformation.h"

class RotationXTransformation : public AxisRotationTransformation {
protected:
    void updateMatrix() final;
public:
    RotationXTransformation() : AxisRotationTransformation() { this->updateMatrix(); }
    explicit RotationXTransformation(float rotation) : AxisRotationTransformation(rotation) { this->updateMatrix(); }
};


#endif //VORLESUNG2_ROTATIONXTRANSFORMATION_H
