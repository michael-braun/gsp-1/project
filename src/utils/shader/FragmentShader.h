//
// Created by michael on 30.04.19.
//

#ifndef VORLESUNG2_FRAGMENTSHADER_H
#define VORLESUNG2_FRAGMENTSHADER_H


#include "Shader.h"

class FragmentShader : public Shader {
public:
    explicit FragmentShader(const char* shader) : Shader(GL_FRAGMENT_SHADER, shader) {};
};


#endif //VORLESUNG2_FRAGMENTSHADER_H
