//
// Created by michael on 30.04.19.
//

#include <vector>
#include <iostream>
#include "Shader.h"
#include "../error/StringException.h"

Shader::Shader(GLenum shaderType, const char *shader) : id(glCreateShader(shaderType)) {
    glShaderSource(this->id, 1, &shader, NULL);
    glCompileShader(this->id);

    GLint success = 0;
    glGetShaderiv(this->id, GL_COMPILE_STATUS, &success);

    if (success == GL_FALSE) {
        GLint maxLength = 0;
        glGetShaderiv(this->id, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        std::vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(this->id, maxLength, &maxLength, &errorLog[0]);

        std::string data(errorLog.data());

        throw(StringException(data));
    }
}

const GLuint Shader::getId() {
    return this->id;
}

Shader::~Shader() {
    glDeleteShader(this->id);
}
