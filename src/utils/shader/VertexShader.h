//
// Created by michael on 30.04.19.
//

#ifndef VORLESUNG2_VERTEXSHADER_H
#define VORLESUNG2_VERTEXSHADER_H


#include "Shader.h"

class VertexShader : public Shader {
public:
    explicit VertexShader(const char* shader) : Shader(GL_VERTEX_SHADER, shader) {};
};


#endif //VORLESUNG2_VERTEXSHADER_H
