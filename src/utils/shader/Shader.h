//
// Created by michael on 30.04.19.
//

#ifndef VORLESUNG2_SHADER_H
#define VORLESUNG2_SHADER_H


#include <GL/glew.h>

class Shader {
private:
    GLuint id;
public:
    Shader(GLenum shaderType, const char* shader);
    const GLuint getId();
    ~Shader();
};


#endif //VORLESUNG2_SHADER_H
