//
// Created by michael on 16.04.19.
//

#ifndef VORLESUNG2_SIMPLEAPPLICATION_H
#define VORLESUNG2_SIMPLEAPPLICATION_H


#include <iostream>
#include <memory>
#include <vector>
#include "utils/application/Application.h"
#include "utils/Window.h"
#include "utils/event/EventHandler.h"
#include "utils/event/EventManager.h"
#include "utils/shader/VertexShader.h"
#include "utils/shader/FragmentShader.h"
#include "utils/Program.h"
#include "utils/point/Point.h"
#include "utils/triangle/Triangle.h"
#include "utils/buffer/Buffer.h"
#include "utils/vertex-array/VertexArray.h"
#include "utils/triangle/TriangleList.h"
#include "utils/transformation/rotation/RotationTransformation.h"

class SimpleApplication : public Application, public EventHandler, public WindowEventHandler {
    std::unique_ptr<Window> window;
    std::unique_ptr<TriangleList> triangleList;
    std::unique_ptr<TransformationList> triangleTransformation = std::make_unique<TransformationList>();
    std::unique_ptr<RotationTransformation> triangleRotation = std::make_unique<RotationTransformation>();
    std::unique_ptr<EventManager> eventManager = std::make_unique<EventManager>(this);
    float rotationX = 0;
    float rotationY = 0;

public:
    SimpleApplication() {
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            throw("Could not initialize SDL2.");
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        window = std::make_unique<Window>();
        if (window == nullptr) {
            throw("Could not construct window.");
        }

        triangleList = std::make_unique<TriangleList>(*triangleTransformation);
        if (triangleList == nullptr) {
            throw("Could not construct triangleList.");
        }

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
    }

    void onExit(const SDL_QuitEvent& event) {
        this->exit(EXIT_SUCCESS);
    }

    void onKeyUp(const SDL_KeyboardEvent& event) {
        if (event.keysym.sym == SDLK_r) {
            this->window->setBackgroundColor(Color::RED);
        } else if (event.keysym.sym == SDLK_l) {
            this->window->setBackgroundColor(Color::BLACK);
        } else if (event.keysym.sym == SDLK_g) {
            this->window->setBackgroundColor(Color::GREEN);
        } else if (event.keysym.sym == SDLK_b) {
            this->window->setBackgroundColor(Color::BLUE);
        } else if (event.keysym.sym == SDLK_w) {
            this->window->setBackgroundColor(Color::WHITE);
        } else if (event.keysym.sym == SDLK_RIGHT) {
            rotationY += 10;
        } else if (event.keysym.sym == SDLK_LEFT) {
            rotationY -= 10;
        } else if (event.keysym.sym == SDLK_UP) {
            rotationX += 10;
        } else if (event.keysym.sym == SDLK_DOWN) {
            rotationX -= 10;
        }
    }

    void onBeforeSwap() {
        triangleList->draw();
    }

    void setup() {
        window->show();
        std::cout << "Hello, World!" << std::endl;

        window->use(this);

        Point v0(-0.6, -0.6, 0.39);
        Point v1(0.6, -0.6, 0.39);
        Point v2(0.0, -0.6, -0.78);
        Point v3(0.0, 0.6, 0.0);

        // Fill
        Triangle t1 = { v0, v3, v2 };
        Triangle t2 = { v2, v3, v1 };
        Triangle t3 = { v1, v3, v0 };
        Triangle t4 = { v2, v1, v0 };
        triangleList->add(t1);
        triangleList->add(t2);
        triangleList->add(t3);
        triangleList->add(t4);

        triangleTransformation->add(*triangleRotation);

        triangleList->setup();
    }

    void loop() {
        triangleRotation->setRotation(rotationX, rotationY, 0);
        this->eventManager->process();
        this->window->draw();
    }

    ~SimpleApplication() {
        SDL_Quit();
    }
};


#endif //VORLESUNG2_SIMPLEAPPLICATION_H
