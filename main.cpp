#include <iostream>
#include "src/utils/application/Application.h"
#include "src/SimpleApplication.h"
#include "src/utils/error/StringException.h"

int main(int argc, char** argv) {
    try {
        SimpleApplication app;

        return app.run();
    } catch(StringException exception) {
        std::cout << "StringException occurred." << std::endl;
        std::cout << exception.getMessage() << std::endl;
    } catch (...) {
        // Handles freeing the storage
        std::cout << "Exception occurred." << std::endl;
    }
}